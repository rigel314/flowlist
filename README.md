flowlist
======

## About
flowlist is a dataflow "programming language" written in a spice netlist inspired format.

## Examples
* A simple program to create a constant with a value roughly that of pi, and print it to stdout
  ```
  c1 gitlab.com/rigel314/flowlist.Const{Val:3.14159} (p1:0)
  p1 gitlab.com/rigel314/flowlist.Print ()
  ```

## How it works
A `.fl` file defines a _structure_, which consists of _nodes_ and _nets_.  Each _node_ is a builtin primitive node or a different `.fl` file.  A _net_ is a connection between _nodes_.  A _net_ must connect from one output of a _node_ to one or more inputs of other _nodes_.
Go programs which use package flowlist are able to register arbitrary go functions as builtin primitive nodes.

### Templates
Some builtin nodes can have instance parameters, like the type of a constant.
When "calling" a different `.fl` file, the "call" can either treat the `.fl` file as a function, or as the contents of a loop or conditional.

### Builtins
* Const
  * Outputs the value it holds
* Print
  * Calls fmt.Println() on its input

## Internals
An `.fl` file turns into a bunch of _nodes_ and connections(_nets_) between _nodes_.  Each _node_ typically wraps a go function.  A node has it's own goroutine with channels for each input and output.  The _node_ wrapper handles the process of reading all input channels, then calling the function, then writing all output channels.

Outputs can go to more than one place, so each _net_ has a goroutine which copies its single source to its one or more destinations.

## Language Specification
In EBNF:
```
File           = { Node } .
Node           = Ident Ident [ GoCompositeLit ] Outports Semicolon
Outports       = [ "(" Outport { "," Outport } ")" ]
Outport        = [ Ident ":" Number ]
Semicolon      = ( ";" | AutoSemicolon )
AutoSemicolon  = ? Golang rules for automatic semicolon insertion, basically a semicolon is inserted at "/n" unless there's a brace or paren or something that tells the lexer, "more stuff is coming in this expression" ?
Ident          = ( "a" … "z" | "A" … "Z" | "_" ) { IdentContents }
IdentContents  = ? all valid UTF-8 runes besides ('\t', '\n', '\v', '\f', '\r', ' ', U+0085 (NEL), U+00A0 (NBSP)) and ( "{", "}", "(", ")", ":", ";" ) ?
GoCompositeLit = ? Any sequence of UTF-8 runes that, when automatically prefixed with a valid golang identifier, golang's parser.ParseExpr() would return an *ast.CompositeLit ?
```
