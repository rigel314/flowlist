package flowlist

import (
	"fmt"
	"reflect"
)

type Const struct {
	Val interface{}
}

func (c *Const) Call() interface{} {
	if c.Val == nil {
		panic("nil Val")
	}
	return c.Val
}

type Print struct{}

func (c *Print) Call(v interface{}) {
	fmt.Println(v)
}

var blocks map[string]reflect.Type = make(map[string]reflect.Type)

func RegisterCallable(name string, typ interface{}) {
	t := reflect.TypeOf(typ)
	pkg := t.PkgPath()
	if pkg == "" {
		pkg = "gitlab.com/rigel314/flowlist"
	}
	n := pkg + "." + name
	if _, ok := t.MethodByName("Call"); !ok {
		panic("Missing Call() method: " + n)
	}
	if _, ok := blocks[n]; ok {
		panic("already registered: " + n)
	}
	blocks[n] = t
}

func init() {
	RegisterCallable("Const", &Const{})
	RegisterCallable("Print", &Print{})
}

func toCallable(val interface{}) *callable {
	ret := callable{}
	ret.fn = reflect.ValueOf(val).MethodByName("Call")
	return &ret
}
