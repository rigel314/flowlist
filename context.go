package flowlist

import "sync"

type context struct {
	handlers map[chan interface{}][]chan interface{}
	parent   *context
}

func (c *context) addHandler(src, dest chan interface{}) {
	_, ok := c.handlers[src]
	if !ok {
		c.handlers[src] = make([]chan interface{}, 0, 1)
	}
	c.handlers[src] = append(c.handlers[src], dest)
}

// runHandlers runs a goroutine for each connection between nodes.
// this goroutine copies from the input of the connection to every destination of the connection
// these channel writes happen in sub-goroutines for parallism
func (c *context) runHandlers() {
	for src, dests := range c.handlers {
		go func(src chan interface{}, dests []chan interface{}) {
			for {
				val := <-src
				if val == nil {
					panic("nil src")
				}
				var wg sync.WaitGroup
				wg.Add(len(dests))
				for _, d := range dests {
					d := d
					go func() {
						d <- val
					}()
				}
				wg.Wait()
			}
		}(src, dests)
	}
}
