package flowlist

import (
	"fmt"
	"io"
	"reflect"
	"sync"

	"gitlab.com/rigel314/flowlist/parser"
)

type Flow struct {
	s structure
}

func Parse(filename string, r io.Reader) (*Flow, error) {
	var ret Flow

	p := parser.NewParser(filename, r)
	file := p.ParseFile()
	if file == nil {
		return nil, fmt.Errorf("failed parsing")
	}

	nodemap := make(map[string]*node)

	for _, nod := range file.Nodes {
		typ, ok := blocks[nod.Typ]
		if !ok {
			return nil, fmt.Errorf("unknown type: " + nod.Typ)
		}
		v := reflect.New(typ.Elem())
		if nod.Literal != nil {
			err := parser.UnmarshalLiteral(v.Interface(), nod.Literal)
			if err != nil {
				return nil, err
			}
		}

		fn := v.MethodByName("Call")
		newnod := &node{
			c: &callable{
				fn:   fn,
				name: nod.Name,
			},
			ins:  make([]port, fn.Type().NumIn()),
			outs: make([][]port, fn.Type().NumOut()),
		}
		ret.s.nodes = append(ret.s.nodes, newnod)

		if _, ok := nodemap[nod.Name]; ok {
			return nil, fmt.Errorf("duplicate node name: " + nod.Name)
		}
		nodemap[nod.Name] = newnod
	}

	for _, nod := range file.Nodes {
		for i, outs := range nod.Outports {
			for _, out := range outs {
				if _, ok := nodemap[out.Dest]; !ok {
					return nil, fmt.Errorf("outport destination not found: " + out.Dest)
				}
				nodemap[nod.Name].outs[i] = append(nodemap[nod.Name].outs[i], port{
					c:   nodemap[out.Dest].c,
					idx: out.InPortIndex,
				})
				nodemap[out.Dest].ins[out.InPortIndex] = port{
					c:   nodemap[nod.Name].c,
					idx: i,
				}
			}
		}
	}

	return &ret, nil
}

type Debug struct{}

type callable struct {
	ins  []chan interface{}
	outs []chan interface{}
	fn   reflect.Value
	name string
}

func (c *callable) call(*context) {
	ins := make([]reflect.Value, len(c.ins))
	for i := 0; i < len(c.ins); i++ {
		tmp := <-c.ins[i]
		if tmp == nil {
			panic("nil inport")
		}
		ins[i] = reflect.ValueOf(tmp)
	}
	outs := c.fn.Call(ins)
	for i := 0; i < len(c.outs); i++ {
		i := i
		go func() {
			tmp := outs[i].Interface()
			if tmp == nil {
				panic("nil outport")
			}
			c.outs[i] <- tmp
		}()
	}
}

type port struct {
	c   *callable
	idx int
}

type node struct {
	c    *callable
	ins  []port
	outs [][]port
}

type structure struct {
	nodes []*node
	ctx   *context
}

// setup will create all the required channels for the connections between nodes
func (p *structure) setup(ctx *context) {
	p.ctx = &context{
		handlers: make(map[chan interface{}][]chan interface{}),
		parent:   ctx,
	}

	// create a channel for every inport and outport of each callable
	// a callable will wait for all input channels to arrive before running
	// then write to all output channels
	for _, nod := range p.nodes {
		ins := make([]chan interface{}, 0, len(nod.ins))
		outs := make([]chan interface{}, 0, len(nod.outs))
		for range nod.ins {
			ins = append(ins, make(chan interface{}))
		}
		for range nod.outs {
			outs = append(outs, make(chan interface{}))
		}

		nod.c.ins, nod.c.outs = ins, outs
	}

	// register a handler for each connection between nodes
	// the handler will copy from its source to all registered dests
	for _, nod := range p.nodes {
		for i, inport := range nod.ins {
			src := inport.c.outs
			dests := nod.c.ins
			p.ctx.addHandler(src[inport.idx], dests[i])
		}
		for i, outport := range nod.outs {
			for _, dest := range outport {
				dests := dest.c.ins
				src := nod.c.outs
				p.ctx.addHandler(src[i], dests[dest.idx])
			}
		}
	}
}

// run will execute the structure, must call setup() first
// this launches all of the goroutines needed for connections, then the actual node goroutines
func (p *structure) run() {
	go p.ctx.runHandlers()

	var wg sync.WaitGroup
	wg.Add(len(p.nodes))
	for _, chain := range p.nodes {
		chain := chain
		go func() {
			chain.c.call(p.ctx)
			wg.Done()
		}()
	}

	wg.Wait()
}
