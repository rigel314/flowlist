package parser_test

import (
	"strings"
	"testing"

	"gitlab.com/rigel314/flowlist/parser"
)

func TestSimple(t *testing.T) {
	src := `
c1 gitlab.com/rigel314/flowlist.Const{Val:3.14159} (p1:0, p2:0)
p1 gitlab.com/rigel314/flowlist.Print ()
p2 gitlab.com/rigel314/flowlist.Print ()
`
	p := parser.NewParser("", strings.NewReader(src))
	t.Log(p.ParseFile())
}
