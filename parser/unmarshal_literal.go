package parser

import (
	"fmt"
	"reflect"
)

func UnmarshalLiteral(dest, source interface{}) error {
	dst := reflect.Indirect(reflect.ValueOf(dest))
	src := reflect.ValueOf(source)

	if !dst.CanSet() {
		return fmt.Errorf("dest not writeable")
	}

	if dst.Kind() != reflect.Struct {
		return fmt.Errorf("dest not a struct, kind: %v", dst.Kind())
	}
	if src.Kind() != reflect.Struct {
		return fmt.Errorf("source not a struct, kind: %v", src.Kind())
	}

	return copyFields(dst, src)
}

func copyFields(dst, src reflect.Value) error {
	for i := 0; i < dst.NumField(); i++ {
		dstf := indirect(dst.Field(i))
		fname := dst.Type().Field(i).Name
		srcf := indirect(src.FieldByName(fname))

		// skip fields that don't exist in src
		if srcf.Kind() == reflect.Invalid {
			continue
		}

		switch k := srcf.Kind(); k {
		case reflect.Chan, reflect.Func, reflect.Map, reflect.Slice, reflect.Interface:
			return fmt.Errorf("invalid source type %v", k)
		}

		switch {
		case srcf.Kind() == reflect.Struct && dstf.Kind() == reflect.Struct:
			copyFields(dst, src)
			continue
		case dstf.Kind() == reflect.Interface && dstf.NumMethod() == 0,
			srcf.Kind() == dstf.Kind(),
			assignable(dstf.Kind(), srcf.Kind()):
			dstf.Set(srcf)
			continue
		}

		return fmt.Errorf("unhandled types for field: %s, source: %v, dest: %v", fname, srcf.Kind(), dstf.Kind())
	}

	return nil
}

func indirect(v reflect.Value) reflect.Value {
	for {
		pre := v
		v = reflect.Indirect(v)
		if pre == v {
			break
		}
	}
	return v
}

func assignable(dst, src reflect.Kind) bool {
	if dst == src {
		return true
	}
	if isintkind(dst) == isintkind(src) {
		return true
	}
	if isfloatkind(dst) == isfloatkind(src) {
		return true
	}
	if iscomplexkind(dst) == iscomplexkind(src) {
		return true
	}

	return false
}

func isintkind(k reflect.Kind) bool {
	switch k {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64, reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		return true
	}
	return false
}
func isfloatkind(k reflect.Kind) bool {
	switch k {
	case reflect.Float32, reflect.Float64:
		return true
	}
	return false
}
func iscomplexkind(k reflect.Kind) bool {
	switch k {
	case reflect.Complex64, reflect.Complex128:
		return true
	}
	return false
}
