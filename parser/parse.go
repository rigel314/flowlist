package parser

import (
	"io"
	"log"
	"reflect"
	"runtime"
	"strconv"

	goast "go/ast"
	goparser "go/parser"
	gotoken "go/token"

	"gitlab.com/rigel314/flowlist/ast"
	"gitlab.com/rigel314/flowlist/scanner"
	"gitlab.com/rigel314/flowlist/token"
)

type Parser struct {
	s    *scanner.Scanner
	curr scanner.Token
}

func NewParser(filename string, r io.Reader) *Parser {
	ret := &Parser{
		s: scanner.NewScanner(filename, r),
	}

	ret.next()

	return ret
}

func (p *Parser) next() {
	p.curr = *p.s.Scan()
}

func (p *Parser) ParseFile() *ast.File {
	ret := ast.File{}

	for p.curr.Typ != token.EOF {
		node := p.node()
		if node == nil {
			return nil
		}
		ret.Nodes = append(ret.Nodes, node)
	}
	return &ret
}

func (p *Parser) node() *ast.Node {
	ret := ast.Node{}

	if !p.expect(token.IDENT, "node") {
		return nil
	}
	ret.Name = p.curr.Lit

	p.next()

	if !p.expect(token.IDENT, "node") {
		return nil
	}
	ret.Typ = p.curr.Lit

	p.next()

	if p.curr.Typ == token.LITERAL {
		ret.Literal = p.literal()
		if ret.Literal == nil {
			return nil
		}
		p.next()
	}

	for p.curr.Typ == token.LPAREN {
		outports := p.outports()
		if outports == nil {
			return nil
		}

		ret.Outports = append(ret.Outports, outports)
	}

	if !p.expect(token.SEMICOLON, "node") {
		return nil
	}

	p.next()

	return &ret
}

func (p *Parser) outports() []*ast.Outport {
	ret := make([]*ast.Outport, 0)

	if !p.expect(token.LPAREN, "outports") {
		return nil
	}

	p.next()

outportloop:
	for {
		switch p.curr.Typ {
		case token.RPAREN:
			p.next()
			break outportloop

		case token.COMMA:
			p.next()

		case token.IDENT:
			outport := p.outport()
			if outport == nil {
				return nil
			}

			ret = append(ret, outport)

		default:
			file, line := callinfo(1)
			fname, pos := p.s.Pos()
			log.Printf("%s:%d, error parsing %s at: %q:%v, unexpected token %v", file, line, "outports", fname, pos, p.curr.Typ)
		}
	}
	return ret
}

func (p *Parser) outport() *ast.Outport {
	ret := ast.Outport{}

	if !p.expect(token.IDENT, "outport") {
		return nil
	}
	ret.Dest = p.curr.Lit

	p.next()

	if !p.expect(token.COLON, "outport") {
		return nil
	}

	p.next()

	if !p.expect(token.NUMBER, "outport") {
		return nil
	}
	var err error
	ret.InPortIndex, err = strconv.Atoi(p.curr.Lit)
	if err != nil {
		file, line := callinfo(1)
		fname, pos := p.s.Pos()
		log.Printf("%s:%d, error parsing %s at: %q:%v, error parsing NUMBER %q", file, line, "outport", fname, pos, p.curr.Lit)
		return nil
	}

	p.next()

	return &ret
}

func (p *Parser) expect(typ token.Token, name string) bool {
	file, line := callinfo(1)
	fname, pos := p.s.Pos()
	if p.curr.Typ != typ {
		log.Printf("%s:%d, error parsing %s at: %q:%v, expecting %v, got %v", file, line, name, fname, pos, typ, p.curr.Typ)
		return false
	}
	return true
}

func (p *Parser) literal() interface{} {
	fname, pos := p.s.Pos()

	expr, err := goparser.ParseExpr("aoeu" + p.curr.Lit)
	if err != nil {
		file, line := callinfo(1)
		log.Printf("%s:%d, error parsing %s at: %q:%v", file, line, "literal", fname, pos)
		return nil
	}

	comp, ok := expr.(*goast.CompositeLit)
	if !ok {
		file, line := callinfo(1)
		log.Printf("%s:%d, error parsing %s at: %q:%v", file, line, "literal", fname, pos)
	}

	ret := valueFromComposite(comp)
	if ret == nil {
		file, line := callinfo(1)
		log.Printf("%s:%d, error parsing %s at: %q:%v", file, line, "literal", fname, pos)
	}
	return ret
}

func valueFromComposite(comp *goast.CompositeLit) interface{} {
	var fields []reflect.StructField

	var vals []interface{}

	for _, expr := range comp.Elts {
		kv, ok := expr.(*goast.KeyValueExpr)
		if !ok {
			return nil
		}
		name, ok := kv.Key.(*goast.Ident)
		if !ok {
			return nil
		}
		switch v := kv.Value.(type) {
		case *goast.CompositeLit:
			x := valueFromComposite(v)
			fields = append(fields, reflect.StructField{
				Name: name.Name,
				Type: reflect.TypeOf(x),
			})
			vals = append(vals, x)

		case *goast.BasicLit:
			x := valueFromBasic(v)
			fields = append(fields, reflect.StructField{
				Name: name.Name,
				Type: reflect.TypeOf(x),
			})
			vals = append(vals, x)

		default:
			return nil
		}
	}

	typ := reflect.StructOf(fields)

	ret := reflect.New(typ).Elem()
	for i := 0; i < typ.NumField(); i++ {
		ret.Field(i).Set(reflect.ValueOf(vals[i]))
	}
	return ret.Interface()
}

func valueFromBasic(v *goast.BasicLit) interface{} {
	switch v.Kind {
	case gotoken.INT:
		i, err := strconv.ParseInt(v.Value, 0, 64)
		if err == nil {
			return i
		}
		ui, err := strconv.ParseUint(v.Value, 0, 64)
		if err == nil {
			return ui
		}
		return nil

	case gotoken.FLOAT:
		f, err := strconv.ParseFloat(v.Value, 64)
		if err != nil {
			return nil
		}
		return f

	case gotoken.IMAG:
		c, err := strconv.ParseComplex(v.Value, 128)
		if err != nil {
			return nil
		}
		return c

	case gotoken.CHAR:
		c, err := strconv.Unquote(v.Value)
		if err != nil {
			return nil
		}
		return c[0]

	case gotoken.STRING:
		s, err := strconv.Unquote(v.Value)
		if err != nil {
			return nil
		}
		return s

	default:
		return nil
	}
}

func callinfo(depth int) (file string, line int) {
	var ok bool
	_, file, line, ok = runtime.Caller(depth)
	if !ok {
		file = "???"
		line = 0
	}

	return
}
