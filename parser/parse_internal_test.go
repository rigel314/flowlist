package parser

import (
	"fmt"
	"strings"
	"testing"
)

func TestLiteral(t *testing.T) {
	src := `{Val:3.14159}`
	p := NewParser("", strings.NewReader(src))
	t.Log(fmt.Sprintf("%+v", p.literal()))
}
