package scanner

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"strings"
	"unicode"

	"gitlab.com/rigel314/flowlist/token"
)

type Scanner struct {
	r          *bufio.Reader
	fname      string
	pos        Pos
	curr       rune
	insertSemi bool
	record     bool
	recbuf     bytes.Buffer
}

func NewScanner(filename string, r io.Reader) *Scanner {
	ret := &Scanner{
		r:     bufio.NewReader(r),
		fname: filename,
		pos:   Pos{Line: 1, OffsetInLine: 1},
	}
	ret.next()
	if ret.curr == bom {
		ret.next() // ignore BOM at file beginning
	}
	return ret
}

const eofrune = rune(-1)
const bom = 0xFEFF // byte order mark, only permitted as very first character

func (s *Scanner) Pos() (string, Pos) {
	return s.fname, s.pos
}

func (s *Scanner) next() {
	var err error
	s.curr, _, err = s.r.ReadRune()
	if err != nil {
		s.curr = eofrune
		return
	}
	if s.record {
		s.recbuf.WriteRune(s.curr)
	}
	s.pos.OffsetInLine++
	if s.curr == '\n' {
		s.pos.Line++
		s.pos.OffsetInLine = 0
	}
}

func (s *Scanner) startrecording() {
	s.recbuf.Reset()
	s.record = true
}
func (s *Scanner) stoprecording() {
	// s.recbuf = bytes.Buffer{}
	s.record = false
}

func (s *Scanner) Scan() *Token {
	s.skipWhitespace()

	p := s.pos

	s.insertSemi = false
	switch ch := s.curr; {
	case isIdent(ch):
		s.insertSemi = true
		return s.scanIdentifier()
	case isNumber(ch):
		s.insertSemi = true
		return s.scanNumber()
	case ch == '{':
		s.insertSemi = true
		return s.scanLiteral()
	default:
		s.next()
		switch ch {
		case eofrune:
			if s.insertSemi {
				s.insertSemi = false
				return &Token{Typ: token.SEMICOLON, Lit: "\n", Filename: s.fname, Pos: p}
			}
			return &Token{Typ: token.EOF, Lit: "", Filename: s.fname, Pos: p}
		case '\n':
			s.insertSemi = false
			return &Token{Typ: token.SEMICOLON, Lit: "\n", Filename: s.fname, Pos: p}
		case '(':
			return &Token{Typ: token.LPAREN, Lit: "(", Filename: s.fname, Pos: p}
		case ')':
			s.insertSemi = true
			return &Token{Typ: token.RPAREN, Lit: ")", Filename: s.fname, Pos: p}
		case ',':
			return &Token{Typ: token.COMMA, Lit: ",", Filename: s.fname, Pos: p}
		case ':':
			return &Token{Typ: token.COLON, Lit: ":", Filename: s.fname, Pos: p}
		case ';':
			return &Token{Typ: token.SEMICOLON, Lit: ";", Filename: s.fname, Pos: p}
		default:
			return &Token{Typ: token.ILLEGAL, Lit: string(ch), Filename: s.fname, Pos: p}
		}
	}
}

func (s *Scanner) skipWhitespace() {
	for unicode.IsSpace(s.curr) && !(s.insertSemi && s.curr == '\n') {
		s.next()
	}
}

func (s *Scanner) scanIdentifier() *Token {
	p := s.pos
	var buf bytes.Buffer
	for !(unicode.IsSpace(s.curr) || isReserved(s.curr)) {
		buf.WriteRune(s.curr)
		s.next()
	}

	return &Token{Typ: token.IDENT, Lit: buf.String(), Filename: s.fname, Pos: p}
}

func (s *Scanner) scanNumber() *Token {
	p := s.pos
	var buf bytes.Buffer
	for isNumber(s.curr) {
		buf.WriteRune(s.curr)
		s.next()
	}

	return &Token{Typ: token.NUMBER, Lit: buf.String(), Filename: s.fname, Pos: p}
}

func (s *Scanner) scanLiteral() *Token {
	p := s.pos
	s.startrecording()
	bracecount := 0
literalloop:
	for {
		s.next()
		switch s.curr {
		case eofrune:
			break literalloop
		case '\'':
			s.next() // these functions expect to have the open char already consumed
			s.scanRune()
		case '"':
			s.next() // these functions expect to have the open char already consumed
			s.scanString()
		case '`':
			s.next() // these functions expect to have the open char already consumed
			s.scanRawString()
		case '{':
			bracecount++
		case '}':
			if bracecount == 0 {
				break literalloop
			}
			bracecount--
		}
	}

	s.stoprecording()

	s.next()

	return &Token{Typ: token.LITERAL, Lit: "{" + s.recbuf.String(), Filename: s.fname, Pos: p}
}

func digitVal(ch rune) int {
	switch {
	case '0' <= ch && ch <= '9':
		return int(ch - '0')
	case 'a' <= lower(ch) && lower(ch) <= 'f':
		return int(lower(ch) - 'a' + 10)
	}
	return 16 // larger than any legal digit val
}

func lower(ch rune) rune { return ('a' - 'A') | ch } // returns lower-case ch iff ch is ASCII letter

// scanEscape parses an escape sequence where rune is the accepted
// escaped quote. In case of a syntax error, it stops at the offending
// character (without consuming it) and returns false. Otherwise
// it returns true.
func (s *Scanner) scanEscape(quote rune) bool {
	var n int
	var base, max uint32
	switch s.curr {
	case 'a', 'b', 'f', 'n', 'r', 't', 'v', '\\', quote:
		s.next()
		return true
	case '0', '1', '2', '3', '4', '5', '6', '7':
		n, base, max = 3, 8, 255
	case 'x':
		s.next()
		n, base, max = 2, 16, 255
	case 'u':
		s.next()
		n, base, max = 4, 16, unicode.MaxRune
	case 'U':
		s.next()
		n, base, max = 8, 16, unicode.MaxRune
	default:
		// msg := "unknown escape sequence"
		// if s.curr < 0 {
		// 	msg = "escape sequence not terminated"
		// }
		// s.error(offs, msg)
		return false
	}

	var x uint32
	for n > 0 {
		d := uint32(digitVal(s.curr))
		if d >= base {
			// msg := fmt.Sprintf("illegal character %#U in escape sequence", s.curr)
			// if s.curr < 0 {
			// 	msg = "escape sequence not terminated"
			// }
			// s.error(s.offset, msg)
			return false
		}
		x = x*base + d
		s.next()
		n--
	}

	if x > max || 0xD800 <= x && x < 0xE000 {
		// s.error(offs, "escape sequence is invalid Unicode code point")
		return false
	}

	return true
}

func (s *Scanner) scanRune() {
	valid := true
	n := 0
	for {
		ch := s.curr
		if ch == '\n' || ch < 0 {
			// only report error if we don't have one already
			if valid {
				// s.error(offs, "rune literal not terminated")
				valid = false
			}
			break
		}
		s.next()
		if ch == '\'' {
			break
		}
		n++
		if ch == '\\' {
			if !s.scanEscape('\'') {
				valid = false
			}
			// continue to read to closing quote
		}
	}

	// if valid && n != 1 {
	// 	s.error(offs, "illegal rune literal")
	// }
}

func (s *Scanner) scanString() {
	for {
		ch := s.curr
		if ch == '\n' || ch < 0 {
			// s.error(offs, "string literal not terminated")
			break
		}
		s.next()
		if ch == '"' {
			break
		}
		if ch == '\\' {
			s.scanEscape('"')
		}
	}
}

func (s *Scanner) scanRawString() {
	for {
		ch := s.curr
		if ch < 0 {
			// s.error(offs, "raw string literal not terminated")
			break
		}
		s.next()
		if ch == '`' {
			break
		}
	}
}

// Pos hold line/offset information for a token
type Pos struct {
	Line         int
	OffsetInLine int
}

func (t Pos) String() string {
	return fmt.Sprintf("%d:%d", t.Line, t.OffsetInLine)
}

// Token holds one token as lexed
type Token struct {
	Filename string
	Typ      token.Token
	Lit      string
	Pos      Pos
}

func (t Token) String() (s string) {
	s = fmt.Sprintf("%s:%d:%d %v %q", t.Filename, t.Pos.Line, t.Pos.OffsetInLine, t.Typ, t.Lit)
	return
}

func isIdent(r rune) bool {
	if r == '_' ||
		('a' <= r && r <= 'z') ||
		('A' <= r && r <= 'Z') {
		return true
	}
	return false
}

func isNumber(r rune) bool {
	if '0' <= r && r <= '9' {
		return true
	}
	return false
}

func isReserved(r rune) bool {
	return strings.ContainsAny(string(r), "{}():;")
}
