package scanner_test

import (
	"strings"
	"testing"

	"gitlab.com/rigel314/flowlist/scanner"
	"gitlab.com/rigel314/flowlist/token"
)

func TestSimple(t *testing.T) {
	src := `
c1 gitlab.com/rigel314/flowlist.Const{Val:3.14159} (p1:0, p2:0)
p1 gitlab.com/rigel314/flowlist.Print ()
p2 gitlab.com/rigel314/flowlist.Print ()
`
	s := scanner.NewScanner("", strings.NewReader(src))
	tok := &scanner.Token{}
	for tok.Typ != token.EOF {
		tok = s.Scan()
		t.Log(tok)
	}
}

func TestEmbeddedBrace(t *testing.T) {
	src := `
c1 gitlab.com/rigel314/flowlist.Const{Val:"}",Val2:'}',Val3:` + "`}`" + `,Val4:{Val:3.14159}} (p1:0)
p1 gitlab.com/rigel314/flowlist.Print ()
`
	s := scanner.NewScanner("", strings.NewReader(src))
	tok := &scanner.Token{}
	for tok.Typ != token.EOF {
		tok = s.Scan()
		t.Log(tok)
	}
}
