package ast

type File struct {
	Nodes []*Node
}

type Node struct {
	Name     string
	Typ      string
	Literal  interface{}
	Outports [][]*Outport
}

type Outport struct {
	Dest        string
	InPortIndex int
}
