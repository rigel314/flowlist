package token

import "strconv"

// Token holds the enum for the kinds of verilog tokens
type Token int

const (
	ILLEGAL Token = iota
	EOF
	WHITESPACE
	IDENT

	RPAREN
	LPAREN
	COMMA
	COLON
	SEMICOLON

	NUMBER
	LITERAL
)

var tokens = [...]string{
	ILLEGAL:    "ILLEGAL",
	EOF:        "EOF",
	WHITESPACE: "WHITESPACE",
	IDENT:      "IDENT",

	RPAREN:    ")",
	LPAREN:    "(",
	COMMA:     ",",
	COLON:     ":",
	SEMICOLON: ";",

	NUMBER:  "NUMBER",
	LITERAL: "LITERAL",
}

// String returns the string corresponding to the token tok.
func (tok Token) String() string {
	s := ""
	if 0 <= tok && tok < Token(len(tokens)) {
		s = tokens[tok]
	}
	if s == "" {
		s = "token(" + strconv.Itoa(int(tok)) + ")"
	}
	return s
}
