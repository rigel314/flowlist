package flowlist

import (
	"strings"
	"testing"
)

func TestConstPrint(t *testing.T) {
	prnt := toCallable(&Print{})
	cnst := toCallable(&Const{Val: 3.14159})
	p := &structure{
		nodes: []*node{
			{
				c: prnt,
				ins: []port{
					{
						c:   cnst,
						idx: 0,
					},
				},
			},
			{
				c: cnst,
				outs: [][]port{
					{
						{
							c:   prnt,
							idx: 0,
						},
					},
				},
			},
		},
	}
	p.setup(nil)
	p.run()
}

func TestConstPrintMulti(t *testing.T) {
	prnt := toCallable(&Print{})
	prnt2 := toCallable(&Print{})
	cnst := toCallable(&Const{Val: 3.14159})
	p := &structure{
		nodes: []*node{
			{
				c: prnt,
				ins: []port{
					{
						c:   cnst,
						idx: 0,
					},
				},
			},
			{
				c: prnt2,
				ins: []port{
					{
						c:   cnst,
						idx: 0,
					},
				},
			},
			{
				c: cnst,
				outs: [][]port{
					{
						{
							c:   prnt,
							idx: 0,
						},
						{
							c:   prnt2,
							idx: 0,
						},
					},
				},
			},
		},
	}
	p.setup(nil)
	p.run()
}

func TestParseSimple(t *testing.T) {
	src := `c1 gitlab.com/rigel314/flowlist.Const{Val:3.14159} (p1:0)
p1 gitlab.com/rigel314/flowlist.Print ()
`
	f, err := Parse("", strings.NewReader(src))
	if err != nil {
		t.Log(err)
		t.FailNow()
	}
	f.s.setup(nil)
	f.s.run()
}
